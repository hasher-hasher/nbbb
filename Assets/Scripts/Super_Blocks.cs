﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Super_Blocks : MonoBehaviour {
	private GameObject local_gobj;
	private Color local_color;

	public Super_Blocks() {
		this.local_gobj = local_gobj;
		this.local_color = local_color;
	}

	public GameObject go {
		get { return local_gobj; }
		set { local_gobj = value; }
	}

	public Color color {
		get { return local_color; }
		set {
			local_color = value;
			local_gobj.GetComponent<SpriteRenderer> ().color = local_color;
		}
	}
}
