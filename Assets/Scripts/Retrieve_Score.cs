﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Retrieve_Score : MonoBehaviour {

	public string what_to_show;

	void Start() {
		switch (what_to_show) {
		case "highest_score":
			GetComponent<Text> ().text = PlayerPrefs.GetInt ("Highest_Player_Score").ToString();
			break;
		case "score":
			GetComponent<Text> ().text = PlayerPrefs.GetInt ("Last_Score").ToString();
			break;
		default:
			Debug.LogError ("insira uma string para retornar um valor");
			break;
		}
	}
}
