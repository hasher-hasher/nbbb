﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Load_Scenes : MonoBehaviour {

	public void Scene_To_Load(string scene_name) {
		SceneManager.LoadScene(scene_name);
	}
}
