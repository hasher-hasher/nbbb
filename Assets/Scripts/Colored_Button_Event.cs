﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Colored_Button_Event : MonoBehaviour {

	public GameObject prefab;

	public void Clicked() {
		GameObject temp;
		temp = Instantiate (prefab, transform.position, Quaternion.identity);
		temp.GetComponent<SpriteRenderer> ().color = GetComponent<RawImage> ().color;
		temp.GetComponent<SpriteRenderer> ().sortingOrder = 1;
	}
}
