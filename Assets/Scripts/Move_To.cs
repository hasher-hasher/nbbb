﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move_To : MonoBehaviour {

	private bool run = false;

	private Vector2 where_to_go = Vector2.zero;

	public float speed = 0;

	void Update() {
		if (run) {
			transform.position = Vector2.MoveTowards (transform.position, where_to_go, Time.deltaTime * speed);
		}
	}

	public void What_Direction(string direction, string what_object) {
		if (what_object == "player") {
			speed = On_Run_Config_File.init_shot_speed;
		} else if (what_object == "block") {
			speed = On_Run_Config_File.init_block_speed;
		}
		switch (direction) {
		case "up":
			run = true;
			where_to_go = new Vector2 (0f, 1000f);
			break;
		case "down":
			run = true;
			where_to_go = new Vector2 (0f, -1000f);
			break;
		default:
			Debug.LogError ("Argumento Inválido");
			break;
		}
	}
}
