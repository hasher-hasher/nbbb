﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block_Controller : MonoBehaviour {

	public GameObject block_prefab;

	private Super_Blocks block_instance = new Super_Blocks();

	private Coroutine c;

	public float respawn_rate = 0;

	void Start() {
		respawn_rate = On_Run_Config_File.init_respawn_rate;
		c = StartCoroutine (Generator_Controller());
	}

	private Color Random_Color() {
		int random = Random.Range (0, 6);
		switch (random) {
		case 0:
			return Color.red;
			break;
		case 1:
			return Color.green;
			break;
		case 2:
			return Color.blue;
			break;
		case 3:
			return new Vector4(1f, 1f, 0f, 1f);
			break;
		case 4:
			return Color.magenta;
			break;
		case 5:
			return Color.cyan;
			break;
		case 6:
			return Color.white;
			break;
		default:
			Debug.LogError ("Deu Merda");
			return Color.grey;
		}
	}

	IEnumerator Generator_Controller() {
		GameObject temp;
		while (true) {
			temp = Instantiate (block_prefab, transform.position, Quaternion.identity);
			block_instance.go = temp;
			block_instance.color = Random_Color ();
			temp.AddComponent<Move_To> ();
			temp.GetComponent<Move_To> ().What_Direction ("down", "block");
			yield return new WaitForSeconds (respawn_rate);
		}
	}
}
