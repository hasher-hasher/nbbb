﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball_Shot_Config : MonoBehaviour {

	public void Lauch_Shot() {
		GameObject temp;
		GameObject the_ball_origin = GameObject.Find ("Ball");
		temp = Instantiate (the_ball_origin, transform.position, Quaternion.identity);
		temp.tag = "Untagged";
		Destroy (temp.GetComponent<Destructor> ());
		temp.GetComponent<SpriteRenderer> ().color = the_ball_origin.GetComponent<SpriteRenderer> ().color;
		the_ball_origin.GetComponent<SpriteRenderer> ().color = Color.white;
		Destroy (temp.GetComponent<Ball_Shot_Config> ());
		Destroy (temp.GetComponent<Receive_Color> ());
		temp.AddComponent<Move_To>().What_Direction("up", "player");
		temp.AddComponent<Shot> ();
	}
}
