﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This file have parameters that config the game funcionality
*/

public static class On_Run_Config_File {
	public static float init_shot_speed = 15f;
	public static float init_respawn_rate = 1.3f;
	public static float init_block_speed = 1f;

	public static Color Color_Filter(Color color) {
		Vector4 to_return = color;
		if (color.r > 1f) {
			to_return.x = 1f;
		}
		if (color.g > 1) {
			to_return.y = 1f;
		}
		if (color.b > 1f) {
			to_return.z = 1f;
		}
		if (color.a > 1f) {
			to_return.w = 1f;
		}
		return to_return;
	}
}
