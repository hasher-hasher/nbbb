﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instantiated_Colors : MonoBehaviour {

	private Transform target;

	private Color current_color;

	void Start() {
		target = GameObject.FindGameObjectWithTag ("Player").transform;
		current_color = GetComponent<SpriteRenderer> ().color;
	}

	void FixedUpdate () {
		transform.position = Vector2.MoveTowards (transform.position, target.position, 15f * Time.deltaTime);
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "Player") {
			other.GetComponent<Receive_Color> ().Blackened ();
			other.GetComponent<SpriteRenderer> ().color += GetComponent<SpriteRenderer>().color;
			Destroy (gameObject);
			print (other.GetComponent<SpriteRenderer> ().color.linear);
		}
	}
}
