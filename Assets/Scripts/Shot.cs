﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Shot : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other) {
		if (other.GetComponent<SpriteRenderer> ().color == On_Run_Config_File.Color_Filter (GetComponent<SpriteRenderer> ().color) && other.tag == "block") {
			GameObject.Find ("Score Manager").SendMessage ("Update_Score", 1);
			Destroy (other.gameObject);
			Destroy (gameObject);
		} 
		if (other.GetComponent<SpriteRenderer> ().color != On_Run_Config_File.Color_Filter (GetComponent<SpriteRenderer> ().color) && other.tag == "block") {
			Score_Manager.Save_Score ();
			Debug.LogError ("Erro");
			SceneManager.LoadScene ("Lose");
		}
	}
}
