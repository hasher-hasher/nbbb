﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Score_Manager : MonoBehaviour {
	private static Super_Player player_instance = new Super_Player();

	public void Update_Score(int points_to_add) {
		player_instance.current_score += points_to_add;
		GameObject.Find ("Score").GetComponent<Text> ().text = player_instance.current_score.ToString ();
	}

	public static void Save_Score(){
		player_instance.Save_Score ();
	}
}
