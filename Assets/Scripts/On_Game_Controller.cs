﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class On_Game_Controller : MonoBehaviour {
	private float time_running_gameplay = 0;
//	private List<int> time_to_difficult = new List<int>(new int[]{
//		6,
//		10,
//		15
//	});

	void Update() {
		time_running_gameplay += Time.deltaTime;
//		if (time_to_difficult.IndexOf((int)time_running_gameplay) != -1) {
//			Increase_Difficulty ();
//			time_running_gameplay = 0;
//		}
		if(time_running_gameplay >= 10f) {
			Increase_Difficulty ();
			time_running_gameplay = 0;
		}
	}

	private void Increase_Difficulty() {
		GameObject.Find ("Block Controller").GetComponent<Block_Controller> ().respawn_rate -= 0.06f;
		foreach (GameObject game_object in GameObject.FindGameObjectsWithTag("block")) {
			game_object.GetComponent<Move_To> ().speed += 0.06f;
			print (game_object.GetComponent<Move_To> ().speed);
		}
	}
}
