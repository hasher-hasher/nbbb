﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Receive_Color : MonoBehaviour {

	public void Blackened() {
		if (GetComponent<SpriteRenderer> ().color.r >= 1f && GetComponent<SpriteRenderer> ().color.g >= 1f && GetComponent<SpriteRenderer> ().color.b >= 1f) {
			GetComponent<SpriteRenderer> ().color = Color.black;
		}
	}
}
