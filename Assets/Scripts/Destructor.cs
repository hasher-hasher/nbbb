﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
public class Destructor : MonoBehaviour {

	[SerializeField]
	private string tag;

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == tag) {
			Score_Manager.Save_Score ();
			SceneManager.LoadScene ("Lose");
		}
	}
}
